<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrower extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'identity_id', 'identity_date', 'identity_address', 'img',
        'name', 'birthday', 'address', 'phone', 'created_by', 'updated_by'
    ];

    public function contracts() {
        return $this->hasMany('App\Contract', 'borrower_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }
    
}
