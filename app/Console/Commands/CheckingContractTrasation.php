<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Contract as Contract;
use App\Borrower as Borrower;
use App\Transaction as Transaction;

class CheckingContractTrasation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Contract:CheckingTransations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiem tra cac hop dong can phai thanh toan lai, cac hop dong qua han';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkingContract();
    }

    protected function parseCarbonFromString($date) {
        $array_date_split = explode('/', $date);
        $result =  Carbon::create($array_date_split[2], $array_date_split[1], $array_date_split[0]);
        return $result;
    }

    protected function checkingContract() 
    {
        // lay toan bo danh sach hop dong dang vay
        $allContractActived = Contract::where('contract_status', '=', config('data.CONTRACT_STATUS')['DANG_VAY'])->get();
        // duyet toan bo hop dong
        foreach ($allContractActived as $contract) {
            // kiem tra xem hop dong nay qua han hay chua
            $closestDate = $this->parseCarbonFromString($contract->pay_date)->startOfDay();;
            $now = Carbon::now()->startOfDay();;
            $comparsesContract = $closestDate->diffInDays($now); 
            if ($closestDate->lt($now)) {
                Contract::find($contract->id)->update([
                    'contract_status' => config('data.CONTRACT_STATUS')['QUA_HAN']
                ]);
            }
            // kiem tra ngay dong lai cua hop dong
            $transaction = Transaction::where('contract_id', '=', $contract->id)->orderBy('created_at', 'desc')->first();;
            if (isset($transaction)) {
                // ngay dong lai gan nhat cua hop dong
                $closestTransactionDate = $this->parseCarbonFromString($transaction->pay_date)->startOfDay();;
                // ngay hien tai
                $comparses = $closestTransactionDate->diffInDays($now); 
                if ($closestTransactionDate->lt($now)) {
                    Contract::find($contract->id)->update([
                        'transaction_status' => config('data.TRANSACTION_STATUS')['NO_LAI'],
                        'number_of_overdue_days' => $comparses
                    ]);
                } else if ($closestTransactionDate->gt($now)) {
                    Contract::find($contract->id)->update([
                        'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                        'number_of_overdue_days' => $comparses
                    ]);
                } else if ($closestTransactionDate->eq($now))  {
                    Contract::find($contract->id)->update([
                        'transaction_status' => config('data.TRANSACTION_STATUS')['HOM_NAY'],
                        'number_of_overdue_days' => 0
                    ]);
                }
            }
        }        
    }
}
