<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'borrower_id', 'borrow_date', 'pay_date', 'collaterals', 'borrow_amount', 'extra_date', 'extra_log',
        'rate', 'total_days', 'number_of_day_to_pay', 'total_interest_amount', 
        'borrow_amount_trustly_per_day', 'borrow_amount_trustly', 'number_of_overdue_days',
        'total_paid_amount', 'contract_type', 'contract_status', 'transaction_status', 'note', 'created_by', 'updated_by', 'deleted_by'
    ];

    public function borrower()
    {
        return $this->belongsTo('App\Borrower', 'borrower_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function transations() {
        return $this->hasMany('App\Transaction', 'contract_id', 'id');
    }
}
