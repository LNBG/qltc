<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Borrower as Borrower;

class BorrowerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the screen which display borrowers.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayViewListBorrower()
    {
        $borrowers = Borrower::all();
        return view('borrower.index', [
            'borrowers' => $borrowers
        ]);
    }

    public function getBorrowerByID($id) 
    {
        $borrower = Borrower::find($id);
        return response()->json([ 'status' =>  200, 'borrower' => $borrower]);
    }

    /**
     * Show the screen which create new borrower.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayViewEditBorrower($id)
    {
        $borrower = Borrower::find($id);
        return view('borrower.edit',['borrower' => $borrower]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return \Validator::make($data, [
            'identity_id' => 'required',
            'identity_date' => 'required',
            'identity_address' => 'required',
            'name' => 'required',
            'birthday' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
    }

    public function editBorrower(Request $request) 
    {
        $data = $request->input();
        $validator = $this->validator($data);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $borrower = [
            'identity_id' => $data['identity_id'],
            'identity_date' => $data['identity_date'],
            'identity_address' => $data['identity_address'],
            'name' => $data['name'],
            'birthday' => $data['birthday'],
            'address' => $data['address'],
            'phone' => $data['phone'],
            'updated_by' => \Auth::user()->id
        ];
        Borrower::find($data['id'])->update($borrower);
        return redirect()->route('get_borrowers');
    }    
}