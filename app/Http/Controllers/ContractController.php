<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Contract as Contract;
use App\Borrower as Borrower;
use App\Transaction as Transaction;
use App\Expense as Expense;
use App\Fund as Fund;

class ContractController extends Controller
{
    /**
     * create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the screen which create new borrower.
     *
     * @return \Illuminate\Http\Response
     */
    public function displayViewCreateNewContract()
    {
        $borrowers = Borrower::all();
        return view('contract.create', ['borrowers' => $borrowers]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function borrowerValidator(array $data)
    {
        return \Validator::make($data, [
            'identity_id' => 'required',
            'identity_date' => 'required',
            'identity_address' => 'required',
            'name' => 'required',
            'birthday' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function contractValidator(array $data)
    {
        return \Validator::make($data, [
            'borrow_date' => 'required', // ngay cho vay
            'pay_date' => 'required', // ngay hen tra
            'borrow_amount' => 'required', // so tien vay,
        ]);
    }
    
    /**
     * Tạo hợp đồng mới
     *
     * @param Request $request
     * @return void
     */
    public function createNewContract(Request $request) 
    {
        $data = $request->input();
        // kiem tra khach hang da ton tai hay chua
        $borrower = Borrower::where('identity_id', '=', $data['identity_id'])->first();
        // neu khach hang chua ton tai thi tao moi khach hang
        if ($borrower == null) {
             // validate khach hang
            $borrowerValidator = $this->borrowerValidator($data);
            if ($borrowerValidator->fails()) {
                return redirect()->back()->withErrors($borrowerValidator)->withInput();
            }
            if (request()->image != null) {
                $imageName = time().'.'.request()->image->getClientOriginalExtension();
                request()->image->move(public_path('images'), $imageName);
            } else {
                $imageName = "noimage.png";
            }
            $borrowerInformation = [
                'identity_id' => $data['identity_id'],
                'identity_date' => $data['identity_date'],
                'identity_address' => $data['identity_address'],
                'name' => $data['name'],
                'birthday' => $data['birthday'],
                'address' => $data['address'],
                'phone' => $data['phone'],
                'img' => "/public/images/" . $imageName,
                'created_by' => \Auth::user()->id
            ];

            $borrower = Borrower::create($borrowerInformation);
        }
        
        $contractValidator = $this->contractValidator($data);
        // validate hop dong
        if ($contractValidator->fails()) {
            return redirect()->back()->withErrors($contractValidator)->withInput();
        }
        // parse va dem xem so ngay vay la bao lau
        $array_date1_split = explode('/', $data['borrow_date']);
        $array_date2_split = explode('/', $data['pay_date']);
        $date1 =  Carbon::create($array_date1_split[2], $array_date1_split[1], $array_date1_split[0]);
        $date2 =  Carbon::create($array_date2_split[2], $array_date2_split[1], $array_date2_split[0]);
        $days = $date1->diffInDays($date2);
        $amountTransaction = 0;
        // convert dilimiter , to int to store data
        $borrow_amount = intval(str_replace(',','',$data['borrow_amount']));
        $borow_amount_trustly = $data['borrow_amount_trustly'] != null ? intval(str_replace(',','',$data['borrow_amount_trustly'])) : 0;
        $borow_amount_trustly_per_day = $data['borrow_amount_trustly_per_day'] != null ? intval(str_replace(',','',$data['borrow_amount_trustly_per_day'])) : 0;
        $rate = $data['rate'] != null ? intval(str_replace(',','',$data['rate'])) : 0;
        // truong hop tao hop dong cho vay tra gop va tin chap
        if ($data['number_of_day_to_pay'] == null) {
            $data['number_of_day_to_pay'] = 0;
        }
        $expenseAmount = 0;        
        if ($data['contract_type'] == config('data.CONTRACT_TYPE')['TRA_GOP']) {
            if ($data['number_of_day_to_pay'] != 0) {
                $amountTransaction = $borow_amount_trustly_per_day * $data['number_of_day_to_pay'];
            } else {
                $amountTransaction = $data['borrow_amount'];
            }
            $contractInformation = [
                'borrower_id' => $borrower->id,
                'borrow_date' => $data['borrow_date'],
                'pay_date' => $data['pay_date'],
                'borrow_amount' => $borrow_amount,
                'borrow_amount_trustly' => $borow_amount_trustly,
                'total_days' => $days,
                'borrow_amount_trustly_per_day' => $borow_amount_trustly_per_day,
                'total_interest_amount' => $borrow_amount - $borow_amount_trustly,
                'total_paid_amount' => $borrow_amount,
                'number_of_day_to_pay' => $data['number_of_day_to_pay'],
                'contract_type' => $data['contract_type'],
                'contract_status' => config('data.CONTRACT_STATUS')['DANG_VAY'],
                'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                'note' => $data['note'],
                'created_by' => \Auth::user()->id
            ];
            $expenseAmount = $borow_amount_trustly;
        } elseif ($data['contract_type'] == config('data.CONTRACT_TYPE')['TIN_CHAP']) {
            // tao hop dong cho vay the chap
            $totalInterestAmount = $days * $rate * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $totalAmount = $totalInterestAmount + $borrow_amount ;
            $amountTransaction = $rate * $data['number_of_day_to_pay'] * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $contractInformation = [
                'borrower_id' => $borrower->id,
                'borrow_date' => $data['borrow_date'],
                'pay_date' => $data['pay_date'],
                'borrow_amount' => $borrow_amount ,
                'rate' => $rate,
                'total_days' => $days,
                'borrow_amount_trustly' => $borow_amount_trustly,
                'number_of_day_to_pay' => $data['number_of_day_to_pay'],
                'total_interest_amount' => $totalInterestAmount,
                'total_paid_amount' => $totalAmount,
                'contract_type' => $data['contract_type'],
                'contract_status' => config('data.CONTRACT_STATUS')['DANG_VAY'],
                'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                'note' => $data['note'],
                'created_by' => \Auth::user()->id
            ];
            $expenseAmount = $borow_amount_trustly;
        } else {
            // tao hop dong cho vay the chap
            $totalInterestAmount = $days * $rate * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $totalAmount = $totalInterestAmount + $borrow_amount ;
            $amountTransaction = $rate * $data['number_of_day_to_pay'] * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $contractInformation = [
                'borrower_id' => $borrower->id,
                'borrow_date' => $data['borrow_date'],
                'pay_date' => $data['pay_date'],
                'collaterals' => $data['collaterals'],
                'borrow_amount' => $borrow_amount ,
                'rate' => $rate,
                'total_days' => $days,
                'number_of_day_to_pay' => $data['number_of_day_to_pay'],
                'borrow_amount_trustly' => $borrow_amount,
                'total_interest_amount' => $totalInterestAmount,
                'total_paid_amount' => $totalAmount,
                'contract_type' => $data['contract_type'],
                'contract_status' => config('data.CONTRACT_STATUS')['DANG_VAY'],
                'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                'note' => $data['note'],
                'created_by' => \Auth::user()->id
            ];
            $expenseAmount = $borrow_amount;
        }

        $contract = Contract::create($contractInformation);
        // tao record ban ghi chi tiêu
        if (isset($contract)) {
            $title = 'Hợp đồng #' . $contract->id;
            $desc = 'Cho khách hàng ' .$borrower->name . ' vay ' .config('data.CONTRACT_TYPE_NAME')[$data['contract_type']] . ' với số tiền: ' . $data['borrow_amount'];
            $dataExpense = [
                'title' => $title,
                'contract_id' => $contract->id,
                'amount' => $expenseAmount,
                'desc' => $desc,
                'created_by' => \Auth::user()->id,
            ];
            Expense::create($dataExpense);
        }

        if ($data['number_of_day_to_pay'] != 0) {
            $pay_date = $date1->addDays($data['number_of_day_to_pay'])->format('d/m/Y');
        } else {
            $pay_date = $data['pay_date'];
        }
        // tao bản ghi log giao dịch xác định ngày đóng tiền và số tiền phải đóng
        $transactionData = [
            'contract_id' => $contract->id,
            'pay_date' => $pay_date,
            'amount' => $amountTransaction,
            'created_by' => \Auth::user()->id
        ];
        Transaction::create($transactionData);
        \Session::flash('flash_message','Tạo hợp đồng thành công!');
        return redirect()->route('list_contracts_summary');
    }    

    /**
     * Thông tin chi tiết hợp đồng theo id 
     *
     * @param integer $id
     * @return void
     */
    public function getDetailContract($id = 0) 
    {
        $contract = Contract::with('borrower')->find($id);
        return view('contract.detail', ['contract' => $contract]);
    }

    /**
     * Danh sách toàn bộ các hợp đồng theo type
     *
     * @param string $type
     * @return void
     */
    public function lists($type = 'tin-chap') {
        if ($type === 'tin-chap') {
            $contractType = config('data.CONTRACT_TYPE')['TIN_CHAP'];
        } elseif ($type === 'the-chap') {
            $contractType = config('data.CONTRACT_TYPE')['THE_CHAP'];
        } elseif ($type === 'tra-gop') {
            $contractType = config('data.CONTRACT_TYPE')['TRA_GOP'];
        }
        $contracts = Contract::where('contract_type', '=', $contractType)->get();

        if ($type === 'tin-chap') {
            return view('contract.tinchap', ['contracts' => $contracts]);
        } elseif ($type === 'the-chap') {
            return view('contract.thechap', ['contracts' => $contracts]);
        } elseif ($type === 'tra-gop') {
            return view('contract.tragop', ['contracts' => $contracts]);
        }
    }

    /**
     * Màn hình hiển thị danh sách hợp đồng đang cho vay
     *
     * @return void
     */
    public function listsSummary()
    {
        $contractsTheChap = Contract::with('borrower')
        ->where('contract_status', '<>', config('data.CONTRACT_STATUS')['THANH_LY'])
        ->where('contract_type', '=', config('data.CONTRACT_TYPE')['THE_CHAP'])->get();
        $contractsTinChap = Contract::with('borrower')
                ->where('contract_status', '<>', config('data.CONTRACT_STATUS')['THANH_LY'])
                ->where('contract_type', '=', config('data.CONTRACT_TYPE')['TIN_CHAP'])->get();
        $contractsTraGop = Contract::with('borrower')
                ->where('contract_status', '<>', config('data.CONTRACT_STATUS')['THANH_LY'])
                ->where('contract_type', '=', config('data.CONTRACT_TYPE')['TRA_GOP'])->get();
                
        return view('contract.index', 
            [
                'contractsTheChap' => $contractsTheChap, 
                'contractsTinChap' => $contractsTinChap,
                'contractsTraGop' => $contractsTraGop,
            ]);
    }

    /**
     * Thanh lý hợp đồng.
     *
     * @param Request $request
     * @return void
     */
    public function completeContract(Request $request) {
        $contractID = $request->contract_id;
        $note = $request->note;

        Contract::find($contractID)->update([
            'note' => $note,
            'contract_status' => 2,
            'updated_by' => \Auth::user()->id
        ]);


        $restTransaction = Transaction::where('contract_id', '=', $contractID)
                                        ->where('paid', '=', 0)->get();

        Transaction::where('contract_id', '=', $contractID)->update([
            'paid' => 1,
            'updated_by' => \Auth::user()->id
        ]);

        $contract = Contract::find($contractID);
        $transactions = Transaction::where('contract_id', '=', $contractID)->get();
        
        if (count($restTransaction) > 0) {
            if ($contract->contract_type == config('data.CONTRACT_TYPE')['THE_CHAP'])
            {
                $titleForFunds = 'Thanh lý hợp đồng thế chấp #' . $contractID;
                Fund::create([
                    'capital_contributor' => 'Tiền lãi định kỳ hợp đồng #' . $contractID,
                    'amount' => $restTransaction[0]->amount,
                    'note' => 'Tiền lãi định kỳ hợp đồng #' . $contractID,
                    'created_by' => \Auth::user()->id
                ]);
            } elseif ($contract->contract_type == config('data.CONTRACT_TYPE')['TRA_GOP']) {
                $titleForFunds = 'Thanh lý hợp đồng trả góp #' . $contractID;
                Fund::create([
                    'capital_contributor' => 'Tiền trả góp định kỳ hợp đồng #' . $contractID,
                    'amount' => $restTransaction[0]->amount,
                    'note' => 'Tiền trả góp định kỳ hợp đồng #' . $contractID,
                    'created_by' => \Auth::user()->id
                ]);
            } elseif ($contract->contract_type == config('data.CONTRACT_TYPE')['TIN_CHAP']) {
                $titleForFunds = 'Thanh lý hợp đồng tín chấp #' . $contractID;
                Fund::create([
                    'capital_contributor' => 'Tiền trả hợp đồng tín chấp #' . $contractID,
                    'amount' => $restTransaction[0]->amount,
                    'note' => 'Tiền trả hợp đồng tín chấp #' . $contractID,
                    'created_by' => \Auth::user()->id
                ]);
            } 
        }
        // lay thong tin hop dong thanh ly
        // tong so tien can phan tra la
        $totalNeedPayAmount = $contract->total_paid_amount;
        //lay thong tin giao dich da dong cua hop dong nay
        $totalPaid = 0;
        foreach ($transactions as $transaction) {
            if ($transaction->paid == 1) {
                $totalPaid += $transaction->amount;
            }
        }
        // tong so tien con phai tra la
        $rest = $totalNeedPayAmount - $totalPaid;
        if ($rest > 0) {
            Fund::create([
                'capital_contributor' => $titleForFunds,
                'amount' => $rest,
                'note' => $titleForFunds,
                'created_by' => \Auth::user()->id
            ]);
        }

        return redirect()->route('get_contract_detail', ['id' => $contractID]);
    }

    public function extraContract(Request $request)
    {
        $contractID = $request->contract_id;
        $extraDate = $request->extra_date;
        $note = $request->note;
        $contract = Contract::find($contractID);
        $oldDate = $contract->pay_date;

        $array_date1_split = explode('/', $extraDate);
        $array_date2_split = explode('/', $contract->pay_date);
        $date1 =  Carbon::create($array_date1_split[2], $array_date1_split[1], $array_date1_split[0]);
        $date2 =  Carbon::create($array_date2_split[2], $array_date2_split[1], $array_date2_split[0]);
        // so ngay gia han
        $days = $date1->diffInDays($date2);
        // tinh tien lai tang them
        $additionalInterestAmount = $request->extra_amount != null ? intval(str_replace(',','',$request->extra_amount)) : 0;
        $totalInterestAmount = $contract->total_interest_amount + $additionalInterestAmount;
        if ($contract->contract_type == config('data.CONTRACT_TYPE')['THE_CHAP']) {
            $totalAmount = $contract->borrow_amount + $totalInterestAmount;
        } else {
            $totalAmount = $contract->total_paid_amount + $additionalInterestAmount;
        }
        $totalDays = $contract->total_days + $days;
        if ($contract->extra_log != null) {
            $note = $contract->extra_log . " - " . $note;
        }
        $contractInformation = [
            'pay_date' => $extraDate,
            'total_days' => $totalDays,
            'extra_date' => $oldDate,
            'total_interest_amount' => $totalInterestAmount,
            'total_paid_amount' => $totalAmount,
            'extra_log' => $note,
            'updated_by' => \Auth::user()->id
        ];

        $contract->update($contractInformation);

        $transationInformation = [
            'contract_id' => $contract->id,
            'pay_date' => $extraDate,
            'amount' => $additionalInterestAmount,
            'created_by' => \Auth::user()->id
        ];

        Transaction::create($transationInformation);
        \Session::flash('flash_message','Gia hạn hợp đồng thành công!');
        return redirect()->route('get_contract_detail', ['id' => $contractID]);
    } 

    protected function parseCarbonFromString($date) {
        $array_date_split = explode('/', $date);
        $result =  Carbon::create($array_date_split[2], $array_date_split[1], $array_date_split[0])->startOfDay();
        return $result;
    }

    protected function confirmPaid(Request $request) {
        $id = $request->id;
        $amount = abs($request->amount != null ? intval(str_replace(',','',$request->amount)) : 0);
        $beforeTransaction = Transaction::find($id);
        $contract = Contract::find($beforeTransaction->contract_id);
        $contractID = $contract->id;
        // update tinh trang giao dich (da dong lai)
        Transaction::find($id)->update([
            'paid' => 1,
            'amount' => $amount
        ]);

        if ($contract->contract_type == config('data.CONTRACT_TYPE')['THE_CHAP'])
        {
            Fund::create([
                'capital_contributor' => 'Tiền lãi định kỳ hợp đồng #' . $contractID,
                'amount' => $amount,
                'note' => 'Tiền lãi định kỳ hợp đồng #' . $contractID,
                'created_by' => \Auth::user()->id
            ]);
        } elseif ($contract->contract_type == config('data.CONTRACT_TYPE')['TRA_GOP']) {
            Fund::create([
                'capital_contributor' => 'Tiền trả góp định kỳ hợp đồng #' . $contractID,
                'amount' => $amount,
                'note' => 'Tiền trả góp định kỳ hợp đồng #' . $contractID,
                'created_by' => \Auth::user()->id
            ]);
        } elseif ($contract->contract_type == config('data.CONTRACT_TYPE')['TIN_CHAP']) {
            Fund::create([
                'capital_contributor' => 'Tiền trả hợp đồng tín chấp #' . $contractID,
                'amount' => $amount,
                'note' => 'Tiền trả hợp đồng tín chấp #' . $contractID,
                'created_by' => \Auth::user()->id
            ]);
        } 
    
        // lay thong tin giao dich
        $lastTransaction = Transaction::where('contract_id', '=', $contractID)->orderBy('created_at', 'desc')->first();
        $expireContract = $this->parseCarbonFromString($contract->pay_date);
        $lastContractPayDate = $this->parseCarbonFromString($lastTransaction->pay_date);
        $beforeTransactionPayDate = $this->parseCarbonFromString($beforeTransaction->pay_date);
        $nextPay = $lastContractPayDate->addDays($contract->number_of_day_to_pay);
        

        $transactions = Transaction::where('contract_id', '=', $contractID)->get();
        $totalPaid = 0;
        foreach ($transactions as $transaction) {
            if ($transaction->paid == 1) {
                $totalPaid += $transaction->amount;
            }
        }
        $completedContract = $contract->total_paid_amount - $totalPaid;
        $amountContinue = $beforeTransaction->amount;
        if ($beforeTransaction->amount > $completedContract)
        {
            $amountContinue = $completedContract;
        }
        
        $canWriteNextTransaction = false;
        if ($beforeTransactionPayDate !=  $expireContract) {
            if ($nextPay->gt($expireContract)) {
                if ($completedContract > 0) {
                    $nextPay = $expireContract;
                    $canWriteNextTransaction = true;
                }
            } else {
                $canWriteNextTransaction = true;
            }
        }

        if ($canWriteNextTransaction && $contract->number_of_day_to_pay > 0 && $completedContract > 0) {
            Transaction::create([
                'contract_id' => $beforeTransaction->contract_id,
                'amount' => $amountContinue,
                'pay_date' => $nextPay->format('d/m/Y'),
                'created_by' => \Auth::user()->id
            ]);
        }
        
        return response()->json(['status' => 200]);
    }

    protected function delete($id) 
    {
        if (\Auth::user()->level != 1) {
            \Session::flash('flash_message','Bạn không có quyền này!');
            return redirect()->route('list_contracts_summary');
        }
        Contract::find($id)->update(['deleted_by' => \Auth::user()->id]);
        Expense::where('contract_id', '=',$id)->delete();
        Contract::find($id)->delete();
        \Session::flash('flash_message','Xoá hợp đồng #' . $id . ' thành công!');
        return redirect()->route('list_contracts_summary');

    }

    protected function edit($id) 
    {
        if (\Auth::user()->level != 1) {
            \Session::flash('flash_message','Bạn không có quyền này!');
            return redirect()->route('list_contracts_summary');
        }
        $contract = Contract::with('borrower')->where('contracts.id','=',$id)->first();
                
        return view('contract.edit', 
            [
                'contract' => $contract, 
            ]);
    }

    protected function editContract(Request $request) 
    {
        $data = $request->input();
        // kiem tra khach hang da ton tai hay chua
        $contractValidator = $this->contractValidator($data);
        // validate hop dong
        if ($contractValidator->fails()) {
            return redirect()->back()->withErrors($contractValidator)->withInput();
        }
        // parse va dem xem so ngay vay la bao lau
        $array_date1_split = explode('/', $data['borrow_date']);
        $array_date2_split = explode('/', $data['pay_date']);
        $date1 =  Carbon::create($array_date1_split[2], $array_date1_split[1], $array_date1_split[0]);
        $date2 =  Carbon::create($array_date2_split[2], $array_date2_split[1], $array_date2_split[0]);
        $days = $date1->diffInDays($date2);
        $amountTransaction = 0;
        // convert dilimiter , to int to store data
        $borrow_amount = intval(str_replace(',','',$data['borrow_amount']));
        $borow_amount_trustly = $data['borrow_amount_trustly'] != null ? intval(str_replace(',','',$data['borrow_amount_trustly'])) : 0;
        $borow_amount_trustly_per_day = $data['borrow_amount_trustly_per_day'] != null ? intval(str_replace(',','',$data['borrow_amount_trustly_per_day'])) : 0;
        $rate = $data['rate'] != null ? intval(str_replace(',','',$data['rate'])) : 0;
        // truong hop tao hop dong cho vay tra gop va tin chap
        if ($data['number_of_day_to_pay'] == null) {
            $data['number_of_day_to_pay'] = 0;
        }
        $expenseAmount = 0;
        if ($data['contract_type'] == config('data.CONTRACT_TYPE')['TRA_GOP']) {
            if ($data['number_of_day_to_pay'] != 0) {
                $amountTransaction = $borow_amount_trustly_per_day * $data['number_of_day_to_pay'];
            } else {
                $amountTransaction = $data['borrow_amount'];
            }
            $contractInformation = [
                'borrow_date' => $data['borrow_date'],
                'pay_date' => $data['pay_date'],
                'borrow_amount' => $borrow_amount,
                'borrow_amount_trustly' => $borow_amount_trustly,
                'total_days' => $days,
                'borrow_amount_trustly_per_day' => $borow_amount_trustly_per_day,
                'total_interest_amount' => $borrow_amount - $borow_amount_trustly,
                'total_paid_amount' => $borrow_amount,
                'number_of_day_to_pay' => $data['number_of_day_to_pay'],
                'contract_type' => $data['contract_type'],
                'contract_status' => config('data.CONTRACT_STATUS')['DANG_VAY'],
                'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                'note' => $data['note'],
                'updated_by' => \Auth::user()->id
            ];
            $expenseAmount = $borow_amount_trustly;
        } elseif ($data['contract_type'] == config('data.CONTRACT_TYPE')['TIN_CHAP']) {
            // tao hop dong cho vay the chap
            $totalInterestAmount = $days * $rate * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $totalAmount = $totalInterestAmount + $borrow_amount ;
            $amountTransaction = $rate * $data['number_of_day_to_pay'] * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $contractInformation = [
                'borrow_date' => $data['borrow_date'],
                'pay_date' => $data['pay_date'],
                'borrow_amount' => $borrow_amount ,
                'rate' => $rate,
                'total_days' => $days,
                'borrow_amount_trustly' => $borow_amount_trustly,
                'number_of_day_to_pay' => $data['number_of_day_to_pay'],
                'total_interest_amount' => $totalInterestAmount,
                'total_paid_amount' => $totalAmount,
                'contract_type' => $data['contract_type'],
                'contract_status' => config('data.CONTRACT_STATUS')['DANG_VAY'],
                'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                'note' => $data['note'],
                'updated_by' => \Auth::user()->id
            ];
            $expenseAmount = $borow_amount_trustly;
        } else {
            // tao hop dong cho vay the chap
            $totalInterestAmount = $days * $rate * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $totalAmount = $totalInterestAmount + $borrow_amount ;
            $amountTransaction = $rate * $data['number_of_day_to_pay'] * $borrow_amount  / config('data.AMOUNT_PER_DAY');
            $contractInformation = [
                'borrow_date' => $data['borrow_date'],
                'pay_date' => $data['pay_date'],
                'collaterals' => $data['collaterals'],
                'borrow_amount' => $borrow_amount,
                'rate' => $rate,
                'total_days' => $days,
                'number_of_day_to_pay' => $data['number_of_day_to_pay'],
                'borrow_amount_trustly' => $borrow_amount,
                'total_interest_amount' => $totalInterestAmount,
                'total_paid_amount' => $totalAmount,
                'contract_type' => $data['contract_type'],
                'contract_status' => config('data.CONTRACT_STATUS')['DANG_VAY'],
                'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                'note' => $data['note'],
                'updated_by' => \Auth::user()->id
            ];
            $expenseAmount = $borrow_amount;
        }

        $contract = Contract::find($request->id)->update($contractInformation);
        Expense::where('contract_id', '=', $request->id)->update([
            'amount' => $expenseAmount,
            'updated_by' => \Auth::user()->id
        ]);
        \Session::flash('flash_message','Cập nhật hợp đồng #' . $request->id . ' thành công!');
        return redirect()->route('list_contracts_summary');
    } 
}