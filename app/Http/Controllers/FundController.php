<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Contract as Contract;
use App\Borrower as Borrower;
use App\Transaction as Transaction;
use App\Fund as Fund;
use App\Expense as Expense;
use PDF;
use Dompdf\Dompdf;

class FundController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $funds = Fund::with(['createdBy', 'updatedBy'])->get();
        return view('fund.index', ['funds' => $funds]);
    }

    public function create(Request $request)
    {
        try {
            $amount = intval(str_replace(",","",$request->amount));
            $data = [
                'capital_contributor' => $request->capital_contributor,
                'amount' => $amount,
                'note' => $request->note,
                'created_by' => \Auth::user()->id
            ];

            Fund::create($data);
            return redirect()->route('fund_index');
        } catch(Exception $ex) {

        }   
    }

    public function viewExpense()
    {
        try {
            $expenses = Expense::all();
            return view('fund.expense',['expenses' => $expenses]);
        } catch (Exception $ex) {

        }
    }   
    
    public function createExpense(Request $request) 
    {
        try {
            $amount = intval(str_replace(",","",$request->amount));
            $data = [
                'title' => $request->title,
                'amount' => $amount,
                'description' => $request->description,
                'note' => $request->note,
                'created_by' => \Auth::user()->id
            ];

            Expense::create($data);
            return redirect()->route('expense');
        } catch (Exception $ex) {

        }
    }

    /** 
    public function sumaryFinance()
    {
        // tong so tien gop von
        $funds = Fund::all(); 

        $totalAmountFunds = 0;
        foreach ($funds as $fund) {
            $totalAmountFunds += $fund->amount;
        }

        // so tien cho vay
        $contracts = Contract::all();
        // tong so tien cho vay
        $totalBorrowAmount = 0;
        // tong so tien se nhan ve
        $totalBorrowAmountWillReceived = 0;
        // tong so tien lai 
        $totalInterestAmount = 0; 
        // tong so tien da nhan tu khach hang
        $totalBorrowAmountReceived = 0;
        // tong so tien chua nhan duoc tu khach hang
        $totalBorrowAmountNotReceived  = 0;


        foreach ($contracts as $contract) {
            if ($contract->contract_type == config('data.CONTRACT_TYPE')['TRA_GOP'])
            {
                $totalBorrowAmount += $contract->borrow_amount_trustly;
            } else {
                $totalBorrowAmount += $contract->borrow_amount;
            }
            $totalInterestAmount += $contract->total_interest_amount;
            $totalBorrowAmountWillReceived += $contract->total_paid_amount;

            $transactions = Transaction::where('contract_id', '=', $contract->id)->get();

            foreach ($transactions as $transaction)
            {
                if ($transaction->paid == 1) {
                    $totalBorrowAmountReceived += $transaction->amount;
                }
            }
        }

        $totalBorrowAmountNotReceived = $totalBorrowAmountWillReceived - $totalBorrowAmountReceived;

        return view('fund.summary', [
            'totalAmountFunds' => $totalAmountFunds,
            'totalBorrowAmount' => $totalBorrowAmount,
            'totalInterestAmount' => $totalInterestAmount,
            'totalBorrowAmountWillReceived' => $totalBorrowAmountWillReceived,
            'totalBorrowAmountReceived' => $totalBorrowAmountReceived,
            'totalBorrowAmountNotReceived' => $totalBorrowAmountNotReceived,
        ]);
    }
    */

    public function summaryFinance() {
        $funds = Fund::with(['createdBy', 'updatedBy'])->get();
        $expenses = Expense::with(['createdBy', 'updatedBy'])->get();

        return view('fund.summary', ['funds' => $funds, 'expenses' => $expenses]);
    }

    public function export2() {
        $funds = Fund::with(['createdBy', 'updatedBy'])->get();
        $expenses = Expense::with(['createdBy', 'updatedBy'])->get();

        #instantiate and use the dompdf class
        $view = \View::make('fund.export2', [
            'funds' => $funds,
            'expenses' => $expenses
        ]);
        $dompdf = new Dompdf();
        $dompdf->set_option('isHtml5ParserEnabled', true);
        $dompdf->loadhtml($view->render());
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser
        return $dompdf->stream();
    }

    public function export() 
    {
        // lay data gop von
        $funds = Fund::all();
        $contracts = Contract::with('borrower')->get();
        // return view('fund.export', [
        //     'funds' => $funds,
        //     'contracts' => $contracts
        // ]);

        #instantiate and use the dompdf class
        $view = \View::make('fund.export', [
            'funds' => $funds,
            'contracts' => $contracts
        ]);
        $dompdf = new Dompdf();
        $dompdf->set_option('isHtml5ParserEnabled', true);
        $dompdf->loadhtml($view->render());
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');
        // Render the HTML as PDF
        $dompdf->render();
        // Output the generated PDF to Browser
        return $dompdf->stream();
        // PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled' => true]);
        // $pdf = PDF::loadView('fund.export', [
        //     'funds' => $funds,
        //     'contracts' => $contracts
        // ]);
        // return $pdf->download('tinhhinhtaichinhsongphong_' . Carbon::now()->format('d_m/_Y' .'pdf'));
    }
}
