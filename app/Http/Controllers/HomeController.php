<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Borrower;
use App\User;
use App\Contract;
use App\Transaction as Transaction;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

        /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';


    protected function parseCarbonFromString($date) {
        $array_date_split = explode('/', $date);
        $result =  Carbon::create($array_date_split[2], $array_date_split[1], $array_date_split[0]);
        return $result;
    }

    protected function checkingContract() 
    {
        // lay toan bo danh sach hop dong dang vay
        $allContractActived = Contract::where('contract_status', '=', config('data.CONTRACT_STATUS')['DANG_VAY'])->get();
        // duyet toan bo hop dong
        foreach ($allContractActived as $contract) {
            // kiem tra xem hop dong nay qua han hay chua
            $closestDate = $this->parseCarbonFromString($contract->pay_date)->startOfDay();;
            $now = Carbon::now()->startOfDay();;
            $comparsesContract = $closestDate->diffInDays($now); 
            if ($closestDate->lt($now)) {
                Contract::find($contract->id)->update([
                    'contract_status' => config('data.CONTRACT_STATUS')['QUA_HAN']
                ]);
            }
            // kiem tra ngay dong lai cua hop dong
            $transaction = Transaction::where('contract_id', '=', $contract->id)->orderBy('created_at', 'desc')->first();;
            if (isset($transaction)) {
                // ngay dong lai gan nhat cua hop dong
                $closestTransactionDate = $this->parseCarbonFromString($transaction->pay_date)->startOfDay();;
                // ngay hien tai
                $comparses = $closestTransactionDate->diffInDays($now); 
                if ($closestTransactionDate->lt($now) && $comparses != 0) {
                    Contract::find($contract->id)->update([
                        'transaction_status' => config('data.TRANSACTION_STATUS')['NO_LAI'],
                        'number_of_overdue_days' => $comparses
                    ]);
                } else if ($closestTransactionDate->gt($now) && $comparses != 0) {
                    Contract::find($contract->id)->update([
                        'transaction_status' => config('data.TRANSACTION_STATUS')['DANG_VAY'],
                        'number_of_overdue_days' => $comparses
                    ]);
                } else if ($closestTransactionDate->eq($now) && $comparses == 0)  {
                    Contract::find($contract->id)->update([
                        'transaction_status' => config('data.TRANSACTION_STATUS')['HOM_NAY'],
                        'number_of_overdue_days' => $comparses
                    ]);
                }
            }
        }        
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        $this->checkingContract();
        return view('home');
    }

    public function login() 
    {
        return view('auth.login');
    }

    protected function authenticated(Request $request)
    {
        $credentials=[
    		'username'=>$request->email,
    		'password'=>$request->password,
        ];
        if (\Auth::attempt($credentials, $request->has('remember'))) {
            return redirect()->route('home');
        } else {
            return back();
        }
    }

    public function register() 
    {
        return view('auth.register');
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $this->middleware('auth');
        $data = $request->input();
        User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'level' => $data['level'],
            'password' => bcrypt($data['password']),
        ]);
        return redirect()->route('employees');
    }

    protected function employees()
    {
        $this->middleware('auth');
        return view('auth.employees', ['employees' => User::where('level', '=', 2)->get()]);
    }

    protected function deleteEmployee($id) {
        $this->middleware('auth');
        User::find($id)->delete();
        return redirect()->route('employees');
    }

    protected function password() {
        return view('auth.passwords.change');
    }

    protected function changePassword(Request $request) {
        $id = \Auth::user()->id;

        User::find($id)->update([
            'password' => bcrypt($request->password)
        ]);
        \Session::flash('flash_message','Đổi mật khẩu thành công');

        return view('auth.passwords.change');
    }

    protected function logout() {
        \Auth::logout();
        return redirect()->route('login');
    }
}
