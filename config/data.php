<?php
// file path: config/data.php
return [
    'CONTRACT_STATUS' => [
        'DANG_VAY' => 1,
        'THANH_LY' => 2,
        'QUA_HAN' => 3,
    ],
    'CONTRACT_STATUS_NAME' => [
        1 => 'Đang vay',
        2 => 'Thanh lý',
        3 => 'Quá hạn',
    ],
    'CONTRACT_STATUS_COLOR' => [
        1 => 'green',
        2 => 'grey',
        3 => 'red',
    ],
    'TRANSACTION_STATUS' => [
        'DANG_VAY' => 1,
        'NO_LAI' => 2,
        'HOM_NAY' => 3,
    ],
    'TRANSACTION_STATUS_NAME' => [
        1 => 'Đang vay',
        2 => 'Nợ lãi',
        3 => 'Hôm nay'
    ],
    'TRANSACTION_STATUS_COLOR' => [
        1 => 'green',
        2 => 'red',
        3 => '#c5a60b',
    ],
    'CONTRACT_TYPE' => [
        'TIN_CHAP' => 1,
        'THE_CHAP' => 2,
        'TRA_GOP' => 3,
    ],
    'AMOUNT_PER_DAY' => 1000000
];