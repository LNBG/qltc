<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('borrower_id'); // id khach hang
            $table->string('borrow_date'); // ngay vay
            $table->string('pay_date'); // ngay tra
            $table->string('extra_date'); // ngay gia han
            $table->string('extra_log'); // history khi gia han hop dong
            $table->string('collaterals'); // tai san the chap
            $table->bigInteger('borrow_amount'); // so tien vay
            $table->integer('rate'); // ti le
            $table->integer('total_days'); // so ngay vay
            $table->integer('number_of_day_to_pay'); // sau bao nhieu ngay dong tien lai 1 lan. 
            $table->bigInteger('total_interest_amount'); // bao tien / 1 trieu / ngay
            $table->bigInteger('total_paid_amount'); // tong so tien phai tra
            $table->integer('contract_type'); // loai hop dong
            $table->integer('contract_status'); // tinh trang hop dong
            $table->integer('created_by'); // nguoi tao
            $table->integer('updated_by'); // nguoi update
            $table->string('note');
            $table->timestamps(); // thoi gian 
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
