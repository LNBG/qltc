@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Danh sách nhân viên</div>
                <div class="panel-body">
                    <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="contract2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Tên nhân viên</th>
                                    <th>Tên đăng nhập</th>
                                    <th>Email</th>
                                    <th>Ngày tạo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $employee)
                                    <tr>
                                        <td> {{ $employee->name }}</td>
                                        <td> {{ $employee->username }}</td>
                                        <td> {{ $employee->email }}</td>
                                        <td> {{ $employee->created_at }}</td>
                                        <td> <a href="{{ route('delete_employee', ['id' => $employee->id]) }}" class="btn btn-primary">Xoá nhân viên</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
