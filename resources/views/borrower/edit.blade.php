@extends('layouts.app') 

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Sửa thông tin khách hàng</div>
            <div class="panel-body">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="POST" action="{{ route('post_borrower_edit') }}">
                        <div class="box-body">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $borrower->id }}" name="id" />
                            <div class="form-group{{ $errors->has('identity_id') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">CMT</label>
                                <div class="col-md-6">
                                    <input id="identity_id" type="text" length="9" class="form-control" name="identity_id" value="{{ $borrower->identity_id }}" required autofocus> 
                                    @if ($errors->has('identity_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('identity_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('identity_date') ? ' has-error' : '' }}">
                                <label for="identity_date" class="col-md-4 control-label">Ngày cấp</label>
                                <div class="col-md-6">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" value="{{ $borrower->identity_date }}" name="identity_date" class="form-control pull-right datepicker">
                                    </div>
                                    @if ($errors->has('identity_date'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('identity_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group{{ $errors->has('identity_address') ? ' has-error' : '' }}">
                                <label for="identity_address" class="col-md-4 control-label">Nơi cấp CMT</label>
                                <div class="col-md-6">
                                    <input id="identity_address" type="text" length="9" class="form-control" name="identity_address" value="{{ $borrower->identity_address }}" required autofocus> 
                                    @if ($errors->has('identity_address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('identity_address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Tên Khách Hàng</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $borrower->name }}" required autofocus> 
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                <label for="birthday" class="col-md-4 control-label">Ngày sinh</label>
                                <div class="col-md-6">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" value="{{ $borrower->birthday }}" name="birthday" class="form-control pull-right datepicker">
                                    </div>
                                    @if ($errors->has('birthday'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Địa chỉ</label>
                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control" name="address" value="{{ $borrower->address }}" required autofocus> 
                                    @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Số điện thoại</label>
                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control" name="phone" value="{{ $borrower->phone }}" required autofocus> 
                                    @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image" class="col-md-4 control-label">Ảnh chứng minh thư</label>
                                <div class="col-md-6">
                                    <img src="{{ $borrower->img }}" width="300" height="200" />
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Cập nhật thông tin khách hàng</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
