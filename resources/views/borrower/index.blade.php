@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Danh sách khách hàng</div>
            <div class="panel-body">
                <div class="box box-primary">
                    <table class="table" id="kh">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên khách hàng</th>
                                <th>Chứng minh thư</th>
                                <th>Địa chỉ</th>
                                <th>Danh sách hợp đồng</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $stt = 0 ?>
                            @foreach($borrowers as $borrower)
                                <tr>
                                    <td>{{ ++$stt }}</td>
                                    <td>{{ $borrower->name }}</td>
                                    <td>{{ $borrower->identity_id }}</td>
                                    <td>{{ $borrower->address }}</td>
                                    <td>
                                        <?php $contracts = $borrower->contracts; ?>
                                        <ol style="padding-left: 0 !important;">
                                        @foreach ($contracts as $contract)
                                            <li><a href="{{ route('get_contract_detail',['id' => $contract->id]) }}">Hợp đồng số #{{ $contract->id }}</a></li>                                            
                                        @endforeach
                                        </ol>
                                    </td>
                                    <td>
                                        <a href="{{ route('get_borrower_edit', ['id' => $borrower->id]) }}" class="btn btn-primary">Sửa</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(function () {
    $('#kh').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
    })
})
</script>
@endsection
