@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('post_contract_create') }}">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Thông tin khách hàng</div>
                    <div class="panel-body">
                        <div class="row" style="padding: 20px;">
                            <label for="lskh" style="margin-bottom: 0; vertical-align: bottom; margin-right: 15px;">Danh sách khách hàng: </label>
                            <select id="lskh" class="form-control select2" style="width: 250px; display:inline-block;">
                                @foreach ($borrowers as $borrower)
                                    <option value="{{ $borrower->id }}">{{ $borrower->name }}</option>
                                @endforeach
                            </select>
                            <button onclick="getBorrowerInformationByID(event)" style="display: inline-block; margin-left: 15px;" class="btn btn-primary">Tạo hợp đồng với khách hàng này</buton>
                            <script>
                                $(".select2").select2();
                            </script>
                        </div>
                        <div id="cus_new" class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-3 control-label">Tên Khách Hàng</label>
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus> @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('identity_id') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-3 control-label">CMT</label>
                                    <div class="col-md-8">
                                        <input id="identity_id" type="text" length="9" class="form-control" name="identity_id" value="{{ old('identity_id') }}" required
                                            autofocus> 
                                        @if ($errors->has('identity_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('identity_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('identity_date') ? ' has-error' : '' }}">
                                    <label for="identity_date" class="col-md-3 control-label">Ngày cấp</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="identity_date" name="identity_date" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                        @if ($errors->has('identity_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('identity_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group{{ $errors->has('identity_address') ? ' has-error' : '' }}">
                                    <label for="identity_address" class="col-md-3 control-label">Nơi cấp CMT</label>
                                    <div class="col-md-8">
                                        <input id="identity_address" type="text" length="9" class="form-control" name="identity_address" value="{{ old('identity_address') }}"
                                            required autofocus> @if ($errors->has('identity_address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('identity_address') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                    <label for="birthday" class="col-md-3 control-label">Ngày sinh</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="birthday" name="birthday" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                        @if ($errors->has('birthday'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('birthday') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label for="address" class="col-md-3 control-label">Địa chỉ</label>
                                    <div class="col-md-8">
                                        <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus> 
                                        @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="col-md-3 control-label">Số điện thoại</label>
                                    <div class="col-md-8">
                                        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus> 
                                        @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label">Ảnh chứng minh</label>
                                    <div class="col-md-8">
                                        <input id="image" type="file" class="form-control" name="image"> 
                                        <br />
                                        <img style="border:1px solid #cecece;" src="images/noimage.png"  width="300" height="200" id="img-preview"  accept=".png, .jpg"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Tạo mới hợp đồng</div>
                    <div class="panel-body">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('borrow_date') ? ' has-error' : '' }}">
                                    <label for="borrow_date" class="col-md-3 control-label">Ngày vay</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"  id="borrow_date"  name="borrow_date" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                        @if ($errors->has('borrow_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('borrow_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('pay_date') ? ' has-error' : '' }}">
                                    <label for="pay_date" class="col-md-3 control-label">Ngày trả</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="pay_date" name="pay_date" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                        @if ($errors->has('pay_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pay_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('contract_type') ? ' has-error' : '' }}">
                                    <label for="contract_type" class="col-md-3 control-label">Loại hợp đồng</label>
                                    <div class="col-md-8">
                                        <select onchange="hienthiformtheohopdong(event)" class="form-control" id="contract_type" name="contract_type" value="{{ old('contract_type') }}" required autofocus>
                                            <option value="1">Vay tín chấp</option>
                                            <option value="2">Vay thế chấp</option>
                                            <option value="3">Trả góp</option>
                                        </select>
                                        @if ($errors->has('contract_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('contract_type') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="thechap-group form-group{{ $errors->has('collaterals') ? ' has-error' : '' }}">
                                    <label for="collaterals" class="col-md-3 control-label">Tài sản thế chấp</label>
                                    <div class="col-md-8">
                                        <input id="collaterals" type="text" class="form-control" name="collaterals" value="{{ old('collaterals') }}"> 
                                        @if ($errors->has('collaterals'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('collaterals') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('borrow_amount') ? ' has-error' : '' }}">
                                    <label for="borrow_amount" class="col-md-3 control-label">Số tiền vay (VND)</label>
                                    <div class="col-md-8">
                                        <input id="borrow_amount" type="text" class="form-control price" name="borrow_amount" value="{{ old('borrow_amount') }}" required autofocus> 
                                        @if ($errors->has('borrow_amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('borrow_amount') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('borrow_amount_trustly') ? ' has-error' : '' }}">
                                    <label for="borrow_amount_trustly" class="col-md-3 control-label">Số tiền thực nhận (VND)</label>
                                    <div class="col-md-8">
                                        <input id="borrow_amount_trustly" onchange="hienthitientragoptrongngay()" type="text" class="form-control price" name="borrow_amount_trustly" value="{{ old('borrow_amount_trustly') }}"> 
                                        @if ($errors->has('borrow_amount_trustly'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('borrow_amount_trustly') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="tragop-group form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-8">
                                        <button type="button" onclick="hienthitientragoptrongngay()" class="btn btn-primary">Tính tiền trả góp / 1 ngày</button>
                                    </div>
                                </div>
                                <div class="tragop-group form-group">
                                    <label for="borrow_amount_trustly_per_day" class="col-md-3 control-label">Tiền trả góp /1 ngày (VND)</label>
                                    <div class="col-md-8">
                                        <input id="borrow_amount_trustly_per_day" type="text" class="form-control price" name="borrow_amount_trustly_per_day" value="">
                                    </div>
                                </div>
                                <input type="hidden" id="so_ngay_tra_gop" name="so_ngay_tra_gop" />
                                <div class="thechap-group form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
                                    <label for="rate" class="col-md-3 control-label">Lãi xuất/1 triệu/1 ngày (VND)</label>
                                    <div class="col-md-8">
                                        <input id="rate" type="text" onchange="tinhtienlaiduavaosovay()" class="form-control price" name="rate" value="{{ old('rate') }}">
                                    </div>
                                </div>
                                <div class="thechap-group form-group">
                                    <label for="rate" class="col-md-3 control-label">Tiền lãi dựa vào số vay/1 ngày (VND)</label>
                                    <div class="col-md-8">
                                        <input id="total_rate_amount" type="text" class="form-control price" name="total_rate_amount" value="">
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('number_of_day_to_pay') ? ' has-error' : '' }}">
                                    <label for="number_of_day_to_pay" class="col-md-3 control-label">Số ngày trả lãi định kỳ</label>
                                    <div class="col-md-8">
                                        <input id="number_of_day_to_pay" type="text" class="form-control" name="number_of_day_to_pay" value="{{ old('number_of_day_to_pay') }}"> 
                                        @if ($errors->has('number_of_day_to_pay'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('number_of_day_to_pay') }}</strong>
                                        </span>
                                        @endif
                                        </br >
                                        <button type="button" onclick="tinhlaicandong(event)" class="thechap-group btn btn-primary">Tính lãi cần đóng</button>
                                    </div>
                                </div>
                                <div class="thechap-group form-group">
                                    <label for="rate" class="col-md-3 control-label">Tiền lãi dựa vào số vay/Số ngày trả lãi định kỳ (VND)</label>
                                    <div class="col-md-8">
                                        <input id="preview-tienlaiphaidong" disabled type="text" class="form-control price" value="">
                                    </div>
                                </div>
                                <input type="hidden" name="pay_dates" />
                                <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                    <label for="note" class="col-md-3 control-label">Ghi chú</label>
                                    <div class="col-md-8">
                                      <textarea class="form-control" name="note" id="note" rows="5" cols="5"></textarea>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-8">
                                        <input type="submit" value="Hoàn thành hợp đồng" class="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<style>
.tragop-group { display: none; }
.cus_old { display: none; }
</style>
<script>
    function GenHTMLNgayTraGop(n) {
        var html = ['<div class="form-group">',
                        '<label for="ngay_tra_gop" class="col-md-3 control-label">Ngày trả góp thứ ', n, '</label>',
                        '<div class="col-md-8"><div class="input-group date"><div class="input-group-addon">',
                        '<i class="fa fa-calendar"></i></div>',
                        '<input type="text" name="ngay_tra_gop_', n ,'" class="form-control pull-right datepicker_ngay_tra_gop">',
                        '</div><input type="text" value="" class="form-control" name="tien_ngay_tra_gop_', n ,'"></div></div>'].join('');
        $("#generate_ngay_tra_gop").append(html);
    }

    function tinhtienlaiduavaosovay() {
        var amount1ngay = $("#rate").val().replace(/,/g , "");
        var borrowAmount = $("#borrow_amount").val().replace(/,/g , "");
        var result = parseInt(amount1ngay) * parseInt(borrowAmount) / 1000000;
        $("#total_rate_amount").val(result);
    }

    function days_between(date1, date2) {
        dt1 = new Date(date1);
        dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

    function tinhlaicandong(event) {
        event.preventDefault();
        // split the date into days, months, years array
        var x = $("#borrow_date").val().split('/')
        var y = $("#pay_date").val().split('/')
        // create date objects using year, month, day
        var a = new Date(x[2],x[1],x[0])
        var b = new Date(y[2],y[1],y[0])
        var diffDays = days_between(a, b);
        var count = $("#number_of_day_to_pay").val();
        var amountnngay = parseInt($("#total_rate_amount").val().replace(/,/g , "")) * count;
        $("#preview-tienlaiphaidong").val(amountnngay);
    }

    function hienthitientragoptrongngay() {
        var x = $("#borrow_date").val().split('/')
        var y = $("#pay_date").val().split('/')
        // create date objects using year, month, day
        var a = new Date(x[2],x[1],x[0])
        var b = new Date(y[2],y[1],y[0])
        var diffDays = days_between(a, b) + 1;
        var borrow_amount = $("#borrow_amount").val().replace(/,/g , "");;
        var amountnngay = parseInt(borrow_amount) / diffDays;
        $("#borrow_amount_trustly_per_day").val(parseInt(amountnngay));

    }

    function hienthiformtheohopdong(event) {
        event.preventDefault();
        var contract_type = $("#contract_type").val();
        if (contract_type == 3) {
            $(".tragop-group").show();
            $(".thechap-group").hide();
        } else {
            $(".thechap-group").show();
            $(".tragop-group").hide();
            $(".tinchap-group").show();
        }
    }

    function getBorrowerInformationByID(event) {
        event.preventDefault();
        var id = $("#lskh").val();
        $.ajax({
            type: 'GET',
            url: '/khach-hang/' + id,
            dataType: 'json',
            success: function(response) {
                var borrower = response.borrower;
                $("#identity_id").val(borrower.identity_id);
                $("#identity_date").val(borrower.identity_date);
                $("#identity_address").val(borrower.identity_address);
                $("#name").val(borrower.name);
                $("#birthday").val(borrower.birthday);
                $("#address").val(borrower.address);
                $("#phone").val(borrower.phone);
                $("#img-preview").attr("src", borrower.img);
            }
        })
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#image").change(function() {
        readURL(this);
    });
</script>
@endsection
