@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
        @endif
        <div class="panel panel-default">
            <div class="panel-heading">Chi tiết hợp đồng - <?php echo $contract->contract_type == 1 ? "Tín Chấp" : $contract->contract_type == 2 ? "Thế chấp" : "Trả Góp" ?></div>
            <div class="panel-body">
                <!-- Thong tin khach hang-->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thông tin khách hàng</h3>
                    </div>
                    <?php $borrower = $contract->borrower;?>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Khách hàng</label>
                            <div class="col-md-8">
                                <label>{{ $borrower->name }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Số Chứng Minh Thư</label>
                            <div class="col-md-8">
                                <label>{{ $borrower->identity_id }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Ngày cấp</label>
                            <div class="col-md-8">
                                <label>{{ $borrower->identity_date }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nơi cấp</label>
                            <div class="col-md-8">
                                <label>{{ $borrower->identity_address }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Ảnh chứng minh thư</label>
                            <div class="col-md-8">
                                <img src="{{ $borrower->img }}" width="300" height="200" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Thong tin hop dong -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thông tin hợp đồng</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Số tiền vay</label>
                            <div class="col-md-8">
                                <label>{{ number_format($contract->borrow_amount) }} VND</label>
                            </div>
                        </div>
                        @if ($contract->contract_type == 3)
                            <div class="form-group">
                                <label class="col-md-3 control-label">Số tiền thực nhận</label>
                                <div class="col-md-8">
                                    <label>{{ number_format($contract->borrow_amount_trustly) }} VND</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ngày vay</label>
                                <div class="col-md-8">
                                    <label>{{ $contract->borrow_date }}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ngày hẹn trả</label>
                                <div class="col-md-8">
                                    <label>{{ $contract->pay_date }}</label>
                                </div>
                            </div>
                            <?php
                                $transactions = App\Transaction::where('contract_id','=', $contract->id)->where('paid', '<>', 0)->get();
                                $paid_amount = 0;
                                foreach ($transactions as $transaction) {
                                    $paid_amount += $transaction->amount;
                                } 
                                $rests = $contract->total_paid_amount - $paid_amount;
                            ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gia hạn(nếu có)</label>
                                <div class="col-md-8">
                                    <label>
                                        @if ($contract->extra_log != null)
                                            {{ $contract->extra_log }}
                                        @else
                                            Không có
                                        @endif
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Đã đóng</label>
                                <div class="col-md-8">
                                    <label>{{ number_format($paid_amount) }} VND</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Còn lại phải đóng</label>
                                <div class="col-md-8">
                                    @if ($contract->contract_status == 2)
                                        <label>0 VND</label>
                                    @else
                                        <label>{{ number_format($rests) }} VND</label>
                                    @endif
                                </div>
                            </div>
                            @if ($contract->contract_status == 2)
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ngày thanh lý</label>
                                <div class="col-md-8">
                                    <label>{{ Carbon\Carbon::parse($contract->updated_at)->format('d-m-Y') }}</label>
                                </div>
                            </div>
                            @endif
                        @else
                            <div class="form-group">
                                <label class="col-md-3 control-label">Số tiền thực nhận</label>
                                <div class="col-md-8">
                                    @if ($contract->borrow_amount_trustly != null)
                                    <label>{{ number_format($contract->borrow_amount_trustly) }} VND</label>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gia hạn(nếu có)</label>
                                <div class="col-md-8">
                                    <label>
                                        @if ($contract->extra_log != null)
                                            {{ $contract->extra_log }}
                                        @else
                                            Không có
                                        @endif
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ngày vay</label>
                                <div class="col-md-8">
                                    <label>{{ $contract->borrow_date }}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ngày hẹn trả</label>
                                <div class="col-md-8">
                                    <label>{{ $contract->pay_date }}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Số tiền phải đóng</label>
                                <div class="col-md-8">
                                    <label>{{ number_format($contract->total_paid_amount) }} VND</label>
                                </div>
                            </div>
                            <?php
                                $transactions = App\Transaction::where('contract_id','=', $contract->id)->where('paid', '<>', 0)->get();
                                $paid_amount = 0;
                                foreach ($transactions as $transaction) {
                                    $paid_amount += $transaction->amount;
                                } 
                                $rests = $contract->total_paid_amount - $paid_amount;
                            ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Đã đóng</label>
                                <div class="col-md-8">
                                    <label>{{ number_format($paid_amount) }} VND</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Còn lại phải đóng</label>
                                <div class="col-md-8">
                                    @if ($contract->contract_status == 2)
                                        <label>0 VND</label>
                                    @else
                                        <label>{{ number_format($rests) }} VND</label>
                                    @endif
                                </div>
                            </div>
                            @if ($contract->contract_status == 2)
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ngày thanh lý</label>
                                <div class="col-md-8">
                                    <label>{{ Carbon\Carbon::parse($contract->updated_at)->format('d-m-Y') }}</label>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tổng tiền lãi</label>
                                <div class="col-md-8">
                                    <label>{{ number_format($contract->total_paid_amount - $contract->borrow_amount) }} VND</label>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <!-- Tinh trang giao dich -->
                <div class="box box-primary">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày trả</th>
                                <th>Tiền trả</th>
                                <th>Tình trạng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $stt = 0 ?>
                            <?php
                                $transactions = App\Transaction::where('contract_id', '=' ,$contract->id)->get();
                                $paid_amount = 0;
                                foreach ($transactions as $transaction) {
                                    $paid_amount += $transaction->amount;
                                } 
                                $rests = $contract->total_paid_amount - $paid_amount;
                            ?>
                            @foreach($transactions as $transaction)
                                <tr class="transaction-row">
                                    <td>{{ ++$stt }}</td>
                                    <td>{{ $transaction->pay_date }}</td>
                                    <td class="amount-col">
                                        @if (\Auth::user()->level == 1 || $transaction->paid == 0)
                                            <input type="text" class="form-control amount-value price" value="{{ $transaction->amount }}" />
                                        @else
                                            <label>{{ number_format($transaction->amount) }}</label>
                                        @endif
                                    </td>
                                    <?php $checked = $transaction->paid != null && $transaction->paid == 1 ? 'checked' : ''; ?>
                                    <td>
                                        @if (\Auth::user()->level == 1 && $transaction->paid == 1)
                                            <label>( Đã thanh toán )</label>
                                        @endif
                                        @if ($transaction->paid == 0 || \Auth::user()->level == 1)
                                            <input type="button" onclick="xacnhandongtien({{ $transaction->id }}, this)" class="btn btn-primary" value="Thanh toán" />
                                        @else 
                                            <input disabled type="button" class="btn btn-primary" value="Đã thanh toán">
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <script>
                        $('.transaction-status').iCheck({
                            checkboxClass: 'icheckbox_flat-green',
                            radioClass   : 'iradio_flat-green'
                        })
                    </script>
                </div>
                @if ($contract->contract_status != 2)
                <div class="box box-primary">
                    <div class="box-body">
                        <form class="form-horizontal" method="POST" action="{{ route('extra_contract') }}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $contract->id }}" name="contract_id" />
                            <div class="form-group">
                                <label for="extra_date" class="col-md-3 control-label">Gia hạn</label>
                                <div class="col-md-8">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="extra_date" name="extra_date" class="form-control pull-right datepicker">
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label for="extra_date" class="col-md-3 control-label">Số tiền thu thêm</label>
                                <div class="col-md-8">
                                    <input type="text" id="extra_amount" name="extra_amount" class="form-control pull-right price">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ghi chú</label>
                                <div class="col-md-8">
                                    <textarea name="note" placeholder="Ví dụ: Gia hạn từ ngày: 01/01/2018 đến ngày: 05/01/2018, số tiền phải đóng thêm khi gia hạn là: 5000000" style="width: 100%" id="note" rows="5" columns="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <button class="btn btn-primary">Gia hạn</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-body">
                        <form class="form-horizontal" method="POST" action="{{ route('post_complete_contract') }}">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{ $contract->id }}" name="contract_id" />
                            <div class="form-group">
                                <label class="col-md-3 control-label">Ghi chú</label>
                                <div class="col-md-8">
                                    <textarea placeholder="Ví dụ: Thanh lý hợp đồng xxx ngày 01/01/2018"  name="note" style="width: 100%" id="note" rows="5" columns="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <button class="btn btn-primary" onclick="thanhlyhopdong()">Thanh lý hợp đồng</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<style>
.box-primary {background:none;}
</style>
<script>
function xacnhandongtien(id, e) {
    var amount = $(e).closest("tr.transaction-row").find("input.amount-value").val().replace(/,/g , "");
    $.ajax({
        url: '/xac-nhan-dong-tien',
        type: 'POST',
        data: {
            "_token": "{{ csrf_token() }}",
            "id": id,
            "amount":parseInt(amount)
        },
        success: function(response) {
            if (response.status == 200) {
                window.location.reload();
            }
        }
    })
}
</script>
@endsection
