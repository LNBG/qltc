@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('post_contract_edit') }}">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Sửa thông tin hợp đồng - <?php echo $contract->contract_type == 1 ? "Tín Chấp" : $contract->contract_type == 2 ? "Thế Chấp" : "Trả Góp" ?></div>
                    <div class="panel-body">
                        <div id="cus_new" class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Thông tin khách hàng</h3>
                            </div>
                            <input type="hidden" name="id" value="{{ $contract->id }}" />
                            <input type="hidden" name="contract_type" value="{{ $contract->contract_type }}" />
                            <!-- /.box-header -->
                            <!-- form start -->
                            <?php $borrower =  $contract->borrower; ?>
                            <div class="box-body">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-md-3 control-label">Tên Khách Hàng</label>
                                    <div class="col-md-8">
                                        <label>{{ $borrower->name }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-md-3 control-label">CMT</label>
                                    <div class="col-md-8">
                                        <label>{{ $borrower->identity_id }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="identity_date" class="col-md-3 control-label">Ngày cấp</label>
                                    <div class="col-md-8">
                                        <label>{{ $borrower->identity_date }}</label>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label for="identity_address" class="col-md-3 control-label">Nơi cấp CMT</label>
                                    <div class="col-md-8">
                                        <label>{{ $borrower->identity_address }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="birthday" class="col-md-3 control-label">Ngày sinh</label>
                                    <div class="col-md-8">
                                        <label>{{ $borrower->birthday }}</label>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-md-3 control-label">Địa chỉ</label>
                                    <div class="col-md-8">
                                        <label>{{ $borrower->address }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label">Số điện thoại</label>
                                    <div class="col-md-8">
                                        <label>{{ $borrower->phone }}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-md-3 control-label">Ảnh chứng minh</label>
                                    <div class="col-md-8">
                                        <img style="border:1px solid #cecece;" src="{{ $borrower->img }}"  width="300" height="200" id="img-preview"  accept=".png, .jpg"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Sửa thông tin hợp đồng</div>
                    <div class="panel-body">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"></h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('borrow_date') ? ' has-error' : '' }}">
                                    <label for="borrow_date" class="col-md-3 control-label">Ngày vay</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" value="{{ $contract->borrow_date }}" id="borrow_date"  name="borrow_date" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                        @if ($errors->has('borrow_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('borrow_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('pay_date') ? ' has-error' : '' }}">
                                    <label for="pay_date" class="col-md-3 control-label">Ngày trả</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" value="{{ $contract->pay_date }}" id="pay_date" name="pay_date" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                        @if ($errors->has('pay_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('pay_date') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="thechap-group form-group{{ $errors->has('collaterals') ? ' has-error' : '' }}">
                                    <label for="collaterals" class="col-md-3 control-label">Tài sản thế chấp</label>
                                    <div class="col-md-8">
                                        <input id="collaterals" type="text" class="form-control" name="collaterals" value="{{ $contract->collaterals }}"> 
                                        @if ($errors->has('collaterals'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('collaterals') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('borrow_amount') ? ' has-error' : '' }}">
                                    <label for="borrow_amount" class="col-md-3 control-label">Số tiền vay (VND)</label>
                                    <div class="col-md-8">
                                        <input id="borrow_amount" type="text" class="form-control price" name="borrow_amount" value="{{ $contract->borrow_amount }}" required autofocus> 
                                        @if ($errors->has('borrow_amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('borrow_amount') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('borrow_amount_trustly') ? ' has-error' : '' }}">
                                    <label for="borrow_amount_trustly" class="col-md-3 control-label">Số tiền thực nhận (VND)</label>
                                    <div class="col-md-8">
                                        <input id="borrow_amount_trustly" onchange="hienthitientragoptrongngay()" type="text" class="form-control price" name="borrow_amount_trustly" value="{{ $contract->borrow_amount_trustly }}"> 
                                        @if ($errors->has('borrow_amount_trustly'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('borrow_amount_trustly') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="tragop-group form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-8">
                                        <button type="button" onclick="hienthitientragoptrongngay()" class="btn btn-primary">Tính tiền trả góp / 1 ngày</button>
                                    </div>
                                </div>
                                <div class="tragop-group form-group">
                                    <label for="borrow_amount_trustly_per_day" class="col-md-3 control-label">Tiền trả góp /1 ngày (VND)</label>
                                    <div class="col-md-8">
                                        <input id="borrow_amount_trustly_per_day" type="text" class="form-control price" name="borrow_amount_trustly_per_day" value="{{ $contract->borrow_amount_trustly_per_day }}">
                                    </div>
                                </div>
                                <input type="hidden" id="so_ngay_tra_gop" name="so_ngay_tra_gop" />
                                <div class="thechap-group form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
                                    <label for="rate" class="col-md-3 control-label">Lãi xuất/1 triệu/1 ngày (VND)</label>
                                    <div class="col-md-8">
                                        <input id="rate" value="{{ $contract->rate }}" type="text" onchange="tinhtienlaiduavaosovay()" class="form-control price" name="rate" />
                                    </div>
                                </div>
                                <div class="thechap-group form-group">
                                    <label for="rate" class="col-md-3 control-label">Tiền lãi dựa vào số vay/1 ngày (VND)</label>
                                    <div class="col-md-8">
                                        <input id="total_rate_amount" type="text" class="form-control price" name="total_rate_amount" value="{{ $contract->total_rate_amount }}">
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('number_of_day_to_pay') ? ' has-error' : '' }}">
                                    <label for="number_of_day_to_pay" class="col-md-3 control-label">Số ngày trả lãi định kỳ</label>
                                    <div class="col-md-8">
                                        <input id="number_of_day_to_pay" type="text" class="form-control" name="number_of_day_to_pay" value="{{ $contract->number_of_day_to_pay }}"> 
                                        @if ($errors->has('number_of_day_to_pay'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('number_of_day_to_pay') }}</strong>
                                        </span>
                                        @endif
                                        </br >
                                        <button type="button" onclick="tinhlaicandong(event)" class="thechap-group btn btn-primary">Tính lãi cần đóng</button>
                                    </div>
                                </div>
                                <div class="thechap-group form-group">
                                    <label for="rate" class="col-md-3 control-label">Tiền lãi dựa vào số vay/Số ngày trả lãi định kỳ (VND)</label>
                                    <div class="col-md-8">
                                        <input id="preview-tienlaiphaidong" disabled type="text" class="form-control price" value="">
                                    </div>
                                </div>
                                <input type="hidden" name="pay_dates" />
                                <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                    <label for="note" class="col-md-3 control-label">Ghi chú</label>
                                    <div class="col-md-8">
                                      <textarea class="form-control" name="note" id="note" rows="5" cols="5"> {{ $contract->note }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-8">
                                        <input type="submit" value="Cập nhật hợp đồng" class="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<style>
@if ($contract->contract_type != config('data.CONTRACT_TYPE')['TRA_GOP'])
.tragop-group { display: none; }
@else
.thechap-group { display: none; }
@endif
.cus_old { display: none; }
</style>
<script>
    function GenHTMLNgayTraGop(n) {
        var html = ['<div class="form-group">',
                        '<label for="ngay_tra_gop" class="col-md-3 control-label">Ngày trả góp thứ ', n, '</label>',
                        '<div class="col-md-8"><div class="input-group date"><div class="input-group-addon">',
                        '<i class="fa fa-calendar"></i></div>',
                        '<input type="text" name="ngay_tra_gop_', n ,'" class="form-control pull-right datepicker_ngay_tra_gop">',
                        '</div><input type="text" value="" class="form-control" name="tien_ngay_tra_gop_', n ,'"></div></div>'].join('');
        $("#generate_ngay_tra_gop").append(html);
    }

    function tinhtienlaiduavaosovay() {
        var amount1ngay = $("#rate").val().replace(/,/g , "");
        var borrowAmount = $("#borrow_amount").val().replace(/,/g , "");
        var result = parseInt(amount1ngay) * parseInt(borrowAmount) / 1000000;
        $("#total_rate_amount").val(result);
    }

    function days_between(date1, date2) {
        dt1 = new Date(date1);
        dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

    function tinhlaicandong(event) {
        event.preventDefault();
        // split the date into days, months, years array
        var x = $("#borrow_date").val().split('/')
        var y = $("#pay_date").val().split('/')
        // create date objects using year, month, day
        var a = new Date(x[2],x[1],x[0])
        var b = new Date(y[2],y[1],y[0])
        var diffDays = days_between(a, b);
        var count = $("#number_of_day_to_pay").val();
        var amountnngay = parseInt($("#total_rate_amount").val().replace(/,/g , "")) * count;
        $("#preview-tienlaiphaidong").val(amountnngay);
    }

    function hienthitientragoptrongngay() {
        var x = $("#borrow_date").val().split('/')
        var y = $("#pay_date").val().split('/')
        // create date objects using year, month, day
        var a = new Date(x[2],x[1],x[0])
        var b = new Date(y[2],y[1],y[0])
        var diffDays = days_between(a, b) + 1;
        var borrow_amount = $("#borrow_amount").val().replace(/,/g , "");;
        var amountnngay = parseInt(borrow_amount) / diffDays;
        $("#borrow_amount_trustly_per_day").val(parseInt(amountnngay));

    }

    function hienthiformtheohopdong(event) {
        event.preventDefault();
        var contract_type = $("#contract_type").val();
        if (contract_type == 3) {
            $(".tragop-group").show();
            $(".thechap-group").hide();
        } else {
            $(".thechap-group").show();
            $(".tragop-group").hide();
            $(".tinchap-group").show();
        }
    }

    function getBorrowerInformationByID(event) {
        event.preventDefault();
        var id = $("#lskh").val();
        $.ajax({
            type: 'GET',
            url: '/khach-hang/' + id,
            dataType: 'json',
            success: function(response) {
                var borrower = response.borrower;
                $("#identity_id").val(borrower.identity_id);
                $("#identity_date").val(borrower.identity_date);
                $("#identity_address").val(borrower.identity_address);
                $("#name").val(borrower.name);
                $("#birthday").val(borrower.birthday);
                $("#address").val(borrower.address);
                $("#phone").val(borrower.phone);
                $("#img-preview").attr("src", borrower.img);
            }
        })
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#image").change(function() {
        readURL(this);
    });
    $(function() {
        tinhtienlaiduavaosovay();
    });
</script>
@endsection
