@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if(Session::has('flash_message'))
            <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
        @endif
        <!-- vay tin chap -->
        <div class="panel panel-default">
            <div class="panel-heading">Hợp đồng cầm đồ</div>
            <div class="panel-body">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="contract1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Mã hợp đồng</th>
                                    <th>Tên Khách Hàng</th>
                                    <th>Số tiền vay (VND)</th>
                                    <th>Lãi suất</th>
                                    <th>Ngày vay</th>
                                    <th>Hẹn trả</th>
                                    <th>Tình trạng hợp đồng</th>
                                    <th>Tình trạng trả lãi</th>
                                    <th>Ngày tiếp theo đóng lãi</th>
                                    @if (\Auth::user()->level == 1)
                                    <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contractsTheChap as $contract)
                                    <?php $borrower = $contract->borrower; ?>
                                    @if (isset($contract))
                                    <tr>
                                        <td><a href="/hop-dong-chi-tiet/{{ $contract->id }}">#{{ $contract->id }}</a></td>
                                        <td>{{ $borrower->name }}</td>
                                        <td>{{ number_format($contract->borrow_amount) }}</td>
                                        <td>{{ number_format($contract->rate) }} VND/ 1triệu/ 1ngày</td>
                                        <td>{{ $contract->borrow_date }}</td>
                                        <td>{{ $contract->pay_date }}</td>
                                        <?php $contract_status_color = config('data.CONTRACT_STATUS_COLOR')[$contract->contract_status]; ?>
                                        <?php $transaction_status_color = config('data.TRANSACTION_STATUS_COLOR')[$contract->transaction_status]; ?>
                                        <?php $contract_status_name = config('data.CONTRACT_STATUS_NAME')[$contract->contract_status]; ?>
                                        <?php $transaction_status_name = config('data.TRANSACTION_STATUS_NAME')[$contract->transaction_status]; ?>
                                        <td>
                                            <span class="td-contract-status" style="background-color:{{ $contract_status_color }};">{{ $contract_status_name }}
                                        </td>
                                        <td>
                                            <span class="td-contract-status" style="background-color:{{ $transaction_status_color }};">{{ $transaction_status_name }}
                                        </td>
                                        <td>
                                            <?php
                                                $transaction = App\Transaction::where('contract_id','=',$contract->id)->where('paid', '=', 0)->first();
                                            ?>
                                            @if (isset($transaction))
                                                {{ $transaction->pay_date }}
                                            @endif
                                        </td>
                                        @if (\Auth::user()->level == 1)
                                        <td><a href="{{ route('edit_contract', ['id' => $contract->id]) }}">Sửa</a></td>
                                        @endif
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- vay tín chấp -->
        <div class="panel panel-default">
            <div class="panel-heading">Hợp đồng vay tín chấp</div>
            <div class="panel-body">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="contract2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Mã hợp đồng</th>
                                    <th>Tên Khách Hàng</th>
                                    <th>Số tiền vay (VND)</th>
                                    <th>Số tiền thực nhận</th>
                                    <th>Lãi suất</th>
                                    <th>Ngày vay</th>
                                    <th>Hẹn trả</th>
                                    <th>Tình trạng hợp đồng</th>
                                    <th>Tình trạng trả lãi</th>
                                    @if (\Auth::user()->level == 1)
                                    <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contractsTinChap as $contract)
                                    <?php $borrower = $contract->borrower; ?>
                                    @if (isset($contract))
                                    <tr>
                                        <td><a href="/hop-dong-chi-tiet/{{ $contract->id }}">#{{ $contract->id }}</a></td>
                                        <td>{{ $borrower->name }}</td>
                                        <td>{{ number_format($contract->borrow_amount) }} VND</td>
                                        <td>{{ number_format($contract->borrow_amount_trustly) }} VND</td>
                                        <td>{{ number_format($contract->rate) }} VND/ 1triệu/ 1ngày</td>
                                        <td>{{ $contract->borrow_date }}</td>
                                        <td>{{ $contract->pay_date }}</td>
                                        <?php $contract_status_color = config('data.CONTRACT_STATUS_COLOR')[$contract->contract_status]; ?>
                                        <?php $transaction_status_color = config('data.TRANSACTION_STATUS_COLOR')[$contract->transaction_status]; ?>
                                        <?php $contract_status_name = config('data.CONTRACT_STATUS_NAME')[$contract->contract_status]; ?>
                                         <?php $transaction_status_name = config('data.TRANSACTION_STATUS_NAME')[$contract->transaction_status]; ?>
                                        <td>
                                            <span class="td-contract-status" style="background-color:{{ $contract_status_color }};">{{ $contract_status_name }}
                                        </td>
                                        <td>
                                            <span class="td-contract-status" style="background-color:{{ $transaction_status_color }};">{{ $transaction_status_name }}
                                        </td>
                                        @if (\Auth::user()->level == 1)
                                        <td><a href="{{ route('edit_contract', ['id' => $contract->id]) }}">Sửa</a></td>
                                        @endif
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- vay trả góp -->
        <div class="panel panel-default">
            <div class="panel-heading">Hợp đồng vay trả góp</div>
            <div class="panel-body">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="contract3" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Mã hợp đồng</th>
                                    <th>Tên Khách Hàng</th>
                                    <th>Số tiền vay (VND)</th>
                                    <th>Số tiền thực nhận</th>
                                    <th>Số tiền trả góp/ 1 ngày</th>
                                    <th>Ngày vay</th>
                                    <th>Hẹn trả</th>
                                    <th>Tình trạng hợp đồng</th>
                                    <th>Tình trạng trả lãi</th>
                                    @if (\Auth::user()->level == 1)
                                    <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contractsTraGop as $contract)
                                    <?php $borrower = $contract->borrower; ?>
                                    @if (isset($contract))
                                    <tr>
                                        <td><a href="/hop-dong-chi-tiet/{{ $contract->id }}">#{{ $contract->id }}</a></td>
                                        <td>{{ $borrower->name }}</td>
                                        <td>{{ number_format($contract->borrow_amount) }} VND</td>
                                        <td>{{ number_format($contract->borrow_amount_trustly) }} VND</td>
                                        <td>{{ number_format($contract->borrow_amount_trustly_per_day) }} VND</td>
                                        <td>{{ $contract->borrow_date }}</td>
                                        <td>{{ $contract->pay_date }}</td>
                                        <?php $contract_status_color = config('data.CONTRACT_STATUS_COLOR')[$contract->contract_status]; ?>
                                        <?php $transaction_status_color = config('data.TRANSACTION_STATUS_COLOR')[$contract->transaction_status]; ?>
                                        <?php $contract_status_name = config('data.CONTRACT_STATUS_NAME')[$contract->contract_status]; ?>
                                         <?php $transaction_status_name = config('data.TRANSACTION_STATUS_NAME')[$contract->transaction_status]; ?>
                                        <td>
                                            <span class="td-contract-status" style="background-color:{{ $contract_status_color }};">{{ $contract_status_name }}
                                        </td>
                                        <td>
                                            <span class="td-contract-status" style="background-color:{{ $transaction_status_color }};">{{ $transaction_status_name }}
                                        </td>
                                        @if (\Auth::user()->level == 1)
                                        <td><a href="{{ route('edit_contract', ['id' => $contract->id]) }}">Sửa</a></td>
                                        @endif
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.td-contract-status {padding: 5px 30px; color: white;}
</style>
<script>
  $(function () {
    $('#contract1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
    })

    $('#contract2').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
    })

    $('#contract3').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
    })
  })
</script>
@endsection
