@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <!-- vay tin chap -->
        <div class="panel panel-default">
            <div class="panel-heading">Hợp đồng cầm đồ</div>
            <div class="panel-body">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="tinchap" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Mã hợp đồng</th>
                                    <th>Tên Khách Hàng</th>
                                    <th>Số tiền vay (VND)</th>
                                    <th>Lãi suất</th>
                                    <th>Ngày vay</th>
                                    <th>Hẹn trả</th>
                                    <th>Tình trạng hợp đồng</th>
                                    <th>Ngày tiếp theo đóng lãi</th>
                                    @if (\Auth::user()->level == 1)
                                    <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contracts as $contract)
                                    <?php $borrower =  $contract->borrower; ?>
                                    @if (isset($contract))
                                    <tr>
                                        <td><a href="/hop-dong-chi-tiet/{{ $contract->id }}">#{{ $contract->id }}</a></td>
                                        <td>{{ $borrower->name }}</td>
                                        <td>{{ number_format($contract->borrow_amount) }}</td>
                                        <td>{{ number_format($contract->rate) }} VND/ 1triệu/ 1ngày</td>
                                        <td>{{ $contract->borrow_date }}</td>
                                        <td>{{ $contract->pay_date }}</td>
                                        <?php $contract_status_color = config('data.TRANSACTION_STATUS_COLOR')[$contract->contract_status]; ?>
                                        <?php $contract_status_name = config('data.CONTRACT_STATUS_NAME')[$contract->contract_status]; ?>
                                        <td>
                                            <span class="td-contract-status" style="background-color:{{ $contract_status_color }};">{{ $contract_status_name }}
                                        </td>
                                        <td>
                                            <?php
                                                $transaction = App\Transaction::where('contract_id','=',$contract->id)->whereNull('paid')->first();
                                            ?>
                                            @if (isset($transaction))
                                                {{ $transaction->pay_date }}
                                            @endif
                                        </td>
                                        @if (\Auth::user()->level == 1)
                                        <td>
                                            <a href="{{ route('edit_contract', ['id' => $contract->id]) }}">Sửa</a> | <a onclick="deleteContract({{ $contract->id }})">Xoá</a>
                                            <script>
                                                function deleteContract(id) {
                                                    var result = confirm("Bạn có chắc chắn muốn xoá hợp đồng #" + id);
                                                    if (result) {
                                                        window.location.href = "/xoa-hop-dong/" + id;
                                                    }
                                                }
                                            </script>
                                        </td>
                                        @endif
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.td-contract-status {padding: 5px 30px; color: white;}
</style>
<script>
  $(function () {
    $('#tinchap').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
    })
  })
</script>
@endsection
