@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Thông tin chi tiêu</div>
            <div class="panel-body">
                <div class="box box-primary">
                    <form class="form-horizontal" method="POST" action="{{ route('expense_create') }}">
                        <div class="box-body">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Nội dung chi tiêu</label>
                                <div class="col-md-6">
                                    <input id="title" type="text" length="9" class="form-control" name="title" value="{{ old('title') }}" required autofocus> 
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                <label for="amount" class="col-md-4 control-label">Số tiền chi</label>
                                <div class="col-md-6">
                                    <input id="amount" type="text" length="9" class="form-control" name="amount" value="{{ old('amount') }}" required autofocus> 
                                    @if ($errors->has('amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="note" class="col-md-4 control-label">Ghi chú</label>
                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}"> 
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div class="box box-primary">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Nội dung chi tiêu</th>
                                <th>Số tiền chi</th>
                                <th>Mô tả</th>
                                <th>Người tạo</th>
                                <th>Ngày tạo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $stt = 0 ?>
                            @foreach($expenses as $expense)
                                <tr>
                                    <td>{{ ++$stt }}</td>
                                    <td>{{ $expense->title }}</td>
                                    <td>{{ number_format($expense->amount) }} VND</td>
                                    <td>{{ $expense->description }}</td>
                                    <td>{{ $expense->createdBy->name }}</td>
                                    <td>{{ $expense->created_at->format('d/m/Y') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.box-primary {background:none;}
</style>
@endsection
