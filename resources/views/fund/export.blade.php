<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Song Long</title>
    <!-- Tell the browser to be responsive to screen width -->
     <style>
     * {
         font-size: 9px;
         font-family: 'DejaVu Sans';
     }
     table {
         width: 100%;
         border-collapse: collapse;
     }
     table th {border: 1px solid #cecece; margin: 0; padding: 5px;}
     table td {border: 1px solid #cecece; padding: 0; padding: 5px;}
     </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="padding: 10px;">
            <!-- Main content -->
            <section class="content container-fluid">
                <div class="row" style="margin-top: 20px;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="12" style="text-align:center; text-transform:uppercase;">Bảng theo dõi tình hình kinh doanh Song Phong</th>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>Mã hợp đồng</th>
                                <th>Tên người vay</th>
                                <th>Thông tin cá nhân</th>
                                <th>Số tiền vay</th>
                                <th>Số tiền vay thực nhận</th>
                                <th>Tài sản thế chấp</th>
                                <th>Ngày vay</th>
                                <th>Ngày trả</th>
                                <th>Lãi xuất(/1m/1d)</th>
                                <th>Đã trả</th>
                                <th>Ghi chú</th>
                                <th>Lợi nhuận</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $stt = 1; 
                            $totalBorrowAmount = 0;
                            $totalInterestAmount = 0;
                            $totalPaidAmount = 0;
                            ?>
                            @foreach ($contracts as $contract)
                                <tr>
                                    <?php 
                                        $borrower = $contract->borrower; 
                                        $totalBorrowAmount += $contract->contract_type == 3 ? 
                                                            $contract->borrow_amount_trustly : $contract->borrow_amount;
                                        $totalInterestAmount += $contract->total_interest_amount;
                                    ?>
                                    <td>{{ $stt++ }}</td>
                                    <td>#{{ $contract->id }}</td>
                                    <td>{{ $borrower->name }}</td>
                                    <td>{{ $borrower->address . ', ' . $borrower->phone }}</td>
                                    <td>{{ number_format($contract->borrow_amount) }}</td>
                                    <td>{{ isset($contract->borrow_amount_trustly) 
                                        ? number_format($contract->borrow_amount_trustly) : '' }}</td>
                                    <td>{{ isset($contract->collaterals) ? $contract->collaterals : '' }}</td>
                                    <td>{{ $contract->borrow_date }}</td>
                                    <td>{{ $contract->pay_date }}</td>
                                    <td>{{ isset($contract->rate) ? number_format($contract->rate) : '' }}</td>
                                    <?php 
                                    $transactions = App\Transaction::where('contract_id', '=', $contract->id)->get();
                                    $paid = 0;
                                    foreach ($transactions as $transition)
                                    {
                                        if ($transition->paid == 1) {
                                            $paid += $transition->amount;
                                        }
                                    }
                                    $totalPaidAmount += $paid;
                                    ?>
                                    <td>{{ number_format($paid) }}</td>
                                    <td>{{ $contract->note }}</td>
                                    <td>{{ number_format($contract->total_interest_amount) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="2">Tổng vay: {{ number_format($totalBorrowAmount) }} VND</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Tổng số tiền khách đã trả: {{ number_format($totalPaidAmount) }} VND</td>
                                <td></td>
                                <td>Tổng lợi nhuận dự tính: {{ number_format($totalInterestAmount) }} VND</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
</body>
</html>