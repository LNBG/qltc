<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Song Long</title>
    <!-- Tell the browser to be responsive to screen width -->
    <style>
        * {
            font-size: 10px;
            font-family: 'DejaVu Sans';
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        table th {
            border: 1px solid #cecece;
            margin: 0;
            padding: 5px;
        }

        table td {
            border: 1px solid #cecece;
            padding: 0;
            padding: 5px;
        }
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="padding: 10px;">
            <!-- Main content -->
            <section class="content container-fluid">
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin nguồn tiền vào</h3>
                        </div>
                        <table id="thu" class="table">
                            <thead>
                                <tr>
                                    <th>Nguồn</th>
                                    <th>Số tiền thu</th>
                                    <th>Mô tả</th>
                                    <th>Ngày tạo</th>
                                    <th>Người tạo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalFunds = 0; ?> @foreach ($funds as $fund)
                                <?php $totalFunds += $fund->amount; ?>
                                <tr>
                                    <td>{{ $fund->capital_contributor }}</td>
                                    <td>{{ number_format($fund->amount) }}</td>
                                    <td>{{ $fund->note }}</td>
                                    <td>{{ $fund->created_at }}</td>
                                    <td>{{ $fund->createdBy->name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="box-body">
                            <h4>Tổng số tiền thu: {{ number_format($totalFunds)}} VND</h4>
                        </div>
                    </div>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin nguồn tiền ra</h3>
                        </div>
                        <table id="chi" class="table">
                            <thead>
                                <tr>
                                    <th>Nội dung chi tiêu</th>
                                    <th>Số tiền chi</th>
                                    <th>Mô tả</th>
                                    <th>Ngày tạo</th>
                                    <th>Người tạo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalExpenses = 0;?> @foreach($expenses as $expense)
                                <?php $totalExpenses += $expense->amount; ?>
                                <tr>
                                    <td>{{ $expense->title }}</td>
                                    <td>{{ number_format($expense->amount) }}</td>
                                    <td>{{ $expense->description }}</td>
                                    <td>{{ $expense->created_at }}</td>
                                    <td>{{ $expense->createdBy->name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="box-body">
                            <h4>Tổng số tiền chi: {{ number_format($totalExpenses)}} VND</h4>
                        </div>
                    </div>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">Tổng tiền còn lại: {{ number_format($totalFunds - $totalExpenses) }} VND</h4>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
</body>

</html>