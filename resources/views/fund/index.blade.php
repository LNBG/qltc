@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Thông tin góp vốn</div>
            <div class="panel-body">
                @if (\Auth::user()->level == 1)
                <div class="box box-primary">
                    <form class="form-horizontal" method="POST" action="{{ route('fund_create') }}">
                        <div class="box-body">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('capital_contributor') ? ' has-error' : '' }}">
                                <label for="capital_contributor" class="col-md-4 control-label">Nguồn</label>
                                <div class="col-md-6">
                                    <input id="capital_contributor" type="text" length="9" class="form-control" name="capital_contributor" value="{{ old('capital_contributor') }}" required autofocus> 
                                    @if ($errors->has('capital_contributor'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('capital_contributor') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                <label for="amount" class="col-md-4 control-label">Số tiền</label>
                                <div class="col-md-6">
                                    <input id="amount" type="text" length="9" class="form-control" name="amount" value="{{ old('amount') }}" required autofocus> 
                                    @if ($errors->has('amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('amount') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                                <label for="note" class="col-md-4 control-label">Ghi chú</label>
                                <div class="col-md-6">
                                    <input id="note" type="text" class="form-control" name="note" value="{{ old('note') }}" required autofocus> 
                                    @if ($errors->has('note'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                @endif
                <div class="box box-primary">
                    <table class="table" id="nguon">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Nguồn</th>
                                <th>Số tiền</th>
                                <th>Ghi chú</th>
                                <th>Ngày góp</th>
                                <th>Người tạo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $stt = 0 ?>
                            @foreach($funds as $fund)
                                <tr>
                                    <td>{{ ++$stt }}</td>
                                    <td>{{ $fund->capital_contributor }}</td>
                                    <td>{{ number_format($fund->amount) }} VND</td>
                                    <td>{{ $fund->note }}</td>
                                    <td>{{ $fund->created_at->format('d/m/Y') }}</td>
                                    <td>{{ $fund->createdBy->name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.box-primary {background:none;}
</style>
<script>
$('#nguon').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
});
</script>
@endsection
