@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Thông tin thu/chi của cửa hàng</div>
            <div class="panel-body">
                <div class="box box-primary">
                    <form class="form-horizontal" id="form-report" method="POST" action="{{ route('make_report') }}">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group" style="max-width: 400px;">
                                    <label for="from_date" class="col-md-3 control-label">Từ ngày</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="from_date" name="from_date" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="form-group" style="max-width: 400px;">
                                    <label for="to_date" class="col-md-3 control-label">Đến ngày</label>
                                    <div class="col-md-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"id="to_date" name="to_date" class="form-control pull-right datepicker" required autofocus>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <button type="submit" style="margin-bottom: 10px;" class="btn btn-primary">Xuất báo cáo theo dõi tình hình kinh doanh</button>            
                            <button type="submit" style="margin-bottom: 10px;" class="btn btn-primary">Xuất báo cáo thống kê thu/chi</button>      
                        </div>
                    </form>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thông tin nguồn tiền vào</h3>
                    </div>
                    <table id="thu" class="table">
                        <thead>
                            <tr>
                                <th>Nguồn</th>
                                <th>Số tiền thu</th>
                                <th>Mô tả</th>
                                <th>Ngày tạo</th>
                                <th>Người tạo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $totalFunds = 0; ?>
                            @foreach ($funds as $fund)
                            <?php $totalFunds += $fund->amount; ?>
                            <tr>
                                <td>{{ $fund->capital_contributor }}</td>
                                <td>{{ number_format($fund->amount) }}</td>
                                <td>{{ $fund->note }}</td>
                                <td>{{ $fund->created_at }}</td>
                                <td>{{ $fund->createdBy->name }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="box-body">
                        <h4>Tổng số tiền thu: {{ number_format($totalFunds)}} VND</h4>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Thông tin nguồn tiền ra</h3>
                    </div>
                    <table id="chi" class="table">
                        <thead>
                            <tr>
                                <th>Nội dung chi tiêu</th>
                                <th>Số tiền chi</th>
                                <th>Mô tả</th>
                                <th>Ngày tạo</th>
                                <th>Người tạo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $totalExpenses = 0;?>
                            @foreach($expenses as $expense)
                            <?php $totalExpenses += $expense->amount; ?>
                            <tr>
                                <td>{{ $expense->title }}</td>
                                <td>{{ number_format($expense->amount) }}</td>
                                <td>{{ $expense->description }}</td>
                                <td>{{ $expense->created_at }}</td>
                                <td>{{ $expense->createdBy->name }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="box-body">
                        <h4>Tổng số tiền chi: {{ number_format($totalExpenses)}} VND</h4>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h4 class="box-title">Tổng tiền còn lại: {{ number_format($totalFunds - $totalExpenses) }} VND</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$('#thu').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
})

$('#chi').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : true
})
</script>
@endsection
