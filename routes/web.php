<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/dang-nhap', 'HomeController@login')->name('login');
Route::post('/dang-nhap', 'HomeController@authenticated')->name('authenticated');
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/tao-moi-nhan-vien', 'HomeController@register')->name('register');
    Route::post('/tao-moi-nhan-vien', 'HomeController@create')->name('create_new_employee');
    Route::get('/danh-sach-nhan-vien', 'HomeController@employees')->name('employees');
    Route::get('/xoa-nhan-vien/{id}', 'HomeController@deleteEmployee')->name('delete_employee');
    Route::get('/khach-hang/{id}', 'BorrowerController@getBorrowerByID')->name('get_borrower_by_id');
    Route::get('/danh-sach-khach-hang', 'BorrowerController@displayViewListBorrower')->name('get_borrowers');
    Route::get('/tao-moi-khach-hang', 'BorrowerController@displayViewCreateNewBorrower')->name('get_borrower_create');
    Route::post('/tao-moi-khach-hang', 'BorrowerController@createNewBorrower')->name('post_borrower_create');
    Route::get('/sua-thong-tin-khach-hang/{id}', 'BorrowerController@displayViewEditBorrower')->name('get_borrower_edit');
    Route::post('/sua-thong-tin-khach-hang', 'BorrowerController@editBorrower')->name('post_borrower_edit');
    Route::get('/tao-moi-hop-dong', 'ContractController@displayViewCreateNewContract')->name('get_contract_create');
    Route::post('/tao-moi-hop-dong', 'ContractController@createNewContract')->name('post_contract_create');
    Route::get('/hop-dong-chi-tiet/{id}', 'ContractController@getDetailContract')->name('get_contract_detail');
    Route::get('/sua-hop-dong/{id}', 'ContractController@edit')->name('edit_contract');
    Route::get('/xoa-hop-dong/{id}', 'ContractController@delete')->name('delete_contract');
    Route::post('/sua-hop-dong', 'ContractController@editContract')->name('post_contract_edit');
    Route::get('/danh-sach-hop-dong/{type}', 'ContractController@lists')->name('list_contracts');
    Route::get('/tinh-trang-danh-sach-hop-dong', 'ContractController@listsSummary')->name('list_contracts_summary');
    Route::post('/thanh-ly-hop-dong', 'ContractController@completeContract')->name('post_complete_contract');
    Route::post('/gia-han-hop-dong', 'ContractController@extraContract')->name('extra_contract');
    Route::post('/xac-nhan-dong-tien', 'ContractController@confirmPaid')->name('confirm_paid');
    Route::get('/nguon-thu', 'FundController@index')->name('fund_index');
    Route::post('/gop-von', 'FundController@create')->name('fund_create');
    Route::get('/thong-ke-tai-chinh', 'FundController@summaryFinance')->name('fund_summary');
    Route::get('/export', 'FundController@export')->name('fund_export');
    Route::get('/export2', 'FundController@export2')->name('fund_export2');
    Route::get('/thong-tin-chi-tieu', 'FundController@viewExpense')->name('expense');
    Route::post('/tao-chi-tieu', 'FundController@createExpense')->name('expense_create');
    Route::get('/password', 'HomeController@password')->name('password');
    Route::post('/password', 'HomeController@changePassword')->name('change_password');
    Route::get('/logout', 'HomeController@logout')->name('logout');
});
